/* replace an image with a flash object */


// create the node for the flash file
function flashNode(flashPath, bgcolor, width, height, name) {
	
	var theImage = document.getElementById("image");
	var theContent = document.getElementById("content");
	
	// create node for the object
	var theFlash = document.createElement('object');
	// assign its attributes
	theFlash.setAttribute("classid","clsid:d27cdb6e-ae6d-11cf-96b8-444553540000");
	theFlash.setAttribute("codebase", "http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0");
	theFlash.setAttribute("id", "flash");
	theFlash.setAttribute("width", width);
	theFlash.setAttribute("height", height);
	
	// create nodes for the different parameters
	// giving each one attributes
	var theParam = document.createElement("param");
	theParam.setAttribute("name", "movie");
	theParam.setAttribute("value", flashPath);
	theFlash.appendChild(theParam);
	
	theParam = document.createElement("param");
	theParam.setAttribute("name", "quality");
	theParam.setAttribute("value", "high");
	theFlash.appendChild(theParam);
	
	theParam = document.createElement("param");
	theParam.setAttribute("name","bgcolor");
	theParam.setAttribute("value", bgcolor);
	theFlash.appendChild(theParam);
	
	// create an embed element node
	// give that node attributes
	theEmbed = document.createElement("embed");
	theEmbed.setAttribute("src", flashPath);
	theEmbed.setAttribute("quality", "high");
	theEmbed.setAttribute("bgcolor", bgcolor);
	theEmbed.setAttribute("swLiveConnect", "false");
	theEmbed.setAttribute("width", width);
	theEmbed.setAttribute("height", height);
	theEmbed.setAttribute("name", name);
	theEmbed.setAttribute("type", "application/x-shockwave-flash");
	theEmbed.setAttribute("pluginspage", "http://www.macromedia.com/go/getflashplayer");
	theFlash.appendChild(theEmbed);
	
	theContent.removeChild(theImage);
	theContent.appendChild(theFlash);
}

// var flashNodeItem = flashNode("flash/company.swf", "#ffffff", 751, 500, "test flash image");

window.onload = function() {
	if(!document.getElementsByTagName) return false;
	flashNode("flash/company.swf", "#ffffff", 751, 500, "test flash image");
}
